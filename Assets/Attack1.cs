﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Attack1 : MonoBehaviour
{
    public Sprite[] attackSprite;
    public float attackSpeed;
    SpriteRenderer spriteRenderer;

    private void Start()
    {
        spriteRenderer = GetComponent<SpriteRenderer>();

        StartAttack();
    }

    public void StartAttack()
    {
        StartCoroutine(Attack());
    }

    IEnumerator Attack()
    {
        for (int i = 0; i < attackSprite.Length; i++)
        {
            spriteRenderer.sprite = attackSprite[i];
            yield return new WaitForEndOfFrame();
        }
        Destroy(gameObject);
    }
}
