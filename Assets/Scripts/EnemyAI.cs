﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyAI : MonoBehaviour
{


    public GameObject player;
    public float dist;
    public float speed = 3;
    public float step;
    public float cd = 1;
    Vector2 point;


    // Use this for initialization
    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player");
    }

    // Update is called once per frame
    void Update()
    {
        dist = Vector2.Distance(player.transform.position, transform.position);
        if (dist <= 15 && dist > 3)
        {
            moveTowards();
        }
        else if (dist <= 3)
        {
            Attack();
        }
    }

    void Attack()
    {
        if (!attackOnCooldown)
        {
            player.GetComponent<PlayerController>().TakeDamage(1);
            StartCoroutine(AttackOnCooldown());
        }
    }

    void moveTowards()
    {
        step = speed * Time.deltaTime;
        point = new Vector2(player.transform.position.x, transform.position.y);
        transform.position = Vector2.MoveTowards(transform.position, point, step);
    }

    public bool attackOnCooldown = false;

    public IEnumerator AttackOnCooldown()
    {
        attackOnCooldown = true;
        yield return new WaitForSeconds(1);
        attackOnCooldown = false;
    }
}
