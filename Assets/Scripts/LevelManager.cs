﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelManager : MonoBehaviour
{

    int roomHeight = 20;
    int roomWidth = 30;


    public int roomCount = 10;

    GameObject[,] rooms;

    public GameObject roomPrefab;


    private static LevelManager instance;

    public static LevelManager Instance
    {
        get { return instance; }
    }

    public float roomSpawnPercentage = 0.5f;
    public float roomDoorOpenChance = 0.3f;

    public void ConstructLevel(int level)
    {
        //TODO Jostain haetaan level statsit

        rooms = new GameObject[roomCount * 2, roomCount * 2];

        int roomsLeft = roomCount;

        

        newCreated.Clear();

        GameObject temp = Instantiate(roomPrefab, transform);
        temp.GetComponent<Room>().posInRoomTable = new Position(roomCount, roomCount);
        rooms[roomCount, roomCount] = temp;
        newCreated.Add(new Position(roomCount, roomCount));
        roomsLeft--;

        createdLastIteration = new List<Position>(newCreated);
        newCreated.Clear();


        while (roomsLeft > 0)
        {
            while (newCreated.Count == 0)
            {
                for (int j = 0; j < createdLastIteration.Count; j++)
                {
                    byte bitValue = CheckPossibleNeighbors(createdLastIteration[j].x, createdLastIteration[j].y);


                    //HACK korjaa ettei pysty kaatumaan ebin
                    if (bitValue == 0)
                    {
                        goto Finish;
                    }

                    if ((bitValue & (1 << 0)) != 0)   //Ylös
                    {
                        if ((Random.Range(0f, 1f) > roomSpawnPercentage) && roomsLeft > 0)
                        {
                            rooms[createdLastIteration[j].x, createdLastIteration[j].y + 1] = Instantiate(roomPrefab, transform);
                            newCreated.Add(new Position(createdLastIteration[j].x, createdLastIteration[j].y + 1));
                            rooms[createdLastIteration[j].x, createdLastIteration[j].y + 1].transform.localPosition = new Vector3((createdLastIteration[j].x - roomCount) * roomWidth, (createdLastIteration[j].y + 1 - roomCount) * roomHeight, 0);
                            roomsLeft--;
                            rooms[createdLastIteration[j].x, createdLastIteration[j].y + 1].GetComponent<Room>().posInRoomTable = new Position(createdLastIteration[j].x, createdLastIteration[j].y + 1);
                            rooms[createdLastIteration[j].x, createdLastIteration[j].y + 1].GetComponent<Room>().OpenDoor(Room.Direction.down);
                            rooms[createdLastIteration[j].x, createdLastIteration[j].y].GetComponent<Room>().OpenDoor(Room.Direction.up);

                        }
                       
                    }
                    else
                    {
                        if (Random.Range(0,1) > roomDoorOpenChance)
                        {

                        rooms[createdLastIteration[j].x, createdLastIteration[j].y + 1].GetComponent<Room>().OpenDoor(Room.Direction.down);
                        rooms[createdLastIteration[j].x, createdLastIteration[j].y].GetComponent<Room>().OpenDoor(Room.Direction.up);
                        }
                    }
                    if ((bitValue & (1 << 1)) != 0)   //Oikee
                    {
                        if ((Random.Range(0f, 1f) > roomSpawnPercentage) && roomsLeft > 0)
                        {
                            rooms[createdLastIteration[j].x + 1, createdLastIteration[j].y] = Instantiate(roomPrefab, transform);
                            newCreated.Add(new Position(createdLastIteration[j].x + 1, createdLastIteration[j].y));
                            rooms[createdLastIteration[j].x + 1, createdLastIteration[j].y].transform.localPosition = new Vector3((createdLastIteration[j].x + 1 - roomCount) * roomWidth, (createdLastIteration[j].y - roomCount) * roomHeight, 0);
                            roomsLeft--;
                            rooms[createdLastIteration[j].x + 1, createdLastIteration[j].y].GetComponent<Room>().posInRoomTable = new Position(createdLastIteration[j].x + 1, createdLastIteration[j].y);
                            rooms[createdLastIteration[j].x + 1, createdLastIteration[j].y].GetComponent<Room>().OpenDoor(Room.Direction.left);
                            rooms[createdLastIteration[j].x, createdLastIteration[j].y].GetComponent<Room>().OpenDoor(Room.Direction.right);

                        }
                    }
                    else
                    {
                        if (Random.Range(0, 1) > roomDoorOpenChance)
                        {
                            rooms[createdLastIteration[j].x + 1, createdLastIteration[j].y].GetComponent<Room>().OpenDoor(Room.Direction.left);
                            rooms[createdLastIteration[j].x, createdLastIteration[j].y].GetComponent<Room>().OpenDoor(Room.Direction.right);
                        }
                    }
                    if ((bitValue & (1 << 2)) != 0)   //Alas
                    {
                        if ((Random.Range(0f, 1f) > roomSpawnPercentage) && roomsLeft > 0)
                        {
                            rooms[createdLastIteration[j].x, createdLastIteration[j].y - 1] = Instantiate(roomPrefab, transform);
                            newCreated.Add(new Position(createdLastIteration[j].x, createdLastIteration[j].y - 1));
                            rooms[createdLastIteration[j].x, createdLastIteration[j].y - 1].transform.localPosition = new Vector3((createdLastIteration[j].x - roomCount) * roomWidth, (createdLastIteration[j].y - 1 - roomCount) * roomHeight, 0);
                            roomsLeft--;
                            rooms[createdLastIteration[j].x, createdLastIteration[j].y - 1].GetComponent<Room>().posInRoomTable = new Position(createdLastIteration[j].x, createdLastIteration[j].y - 1);
                            rooms[createdLastIteration[j].x, createdLastIteration[j].y - 1].GetComponent<Room>().OpenDoor(Room.Direction.up);
                            rooms[createdLastIteration[j].x, createdLastIteration[j].y].GetComponent<Room>().OpenDoor(Room.Direction.down);
                        }
                    }
                    else
                    {
                        if (Random.Range(0, 1) > roomDoorOpenChance)
                        {
                            rooms[createdLastIteration[j].x, createdLastIteration[j].y - 1].GetComponent<Room>().OpenDoor(Room.Direction.up);
                            rooms[createdLastIteration[j].x, createdLastIteration[j].y].GetComponent<Room>().OpenDoor(Room.Direction.down);
                        }
                    }
                    if ((bitValue & (1 << 3)) != 0)   //Vasen
                    {
                        if ((Random.Range(0f, 1f) > roomSpawnPercentage) && roomsLeft > 0)
                        {
                            rooms[createdLastIteration[j].x - 1, createdLastIteration[j].y] = Instantiate(roomPrefab, transform);
                            newCreated.Add(new Position(createdLastIteration[j].x - 1, createdLastIteration[j].y));
                            rooms[createdLastIteration[j].x - 1, createdLastIteration[j].y].transform.localPosition = new Vector3((createdLastIteration[j].x - 1 - roomCount) * roomWidth, (createdLastIteration[j].y - roomCount) * roomHeight, 0);
                            roomsLeft--;
                            rooms[createdLastIteration[j].x - 1, createdLastIteration[j].y].GetComponent<Room>().posInRoomTable = new Position(createdLastIteration[j].x - 1, createdLastIteration[j].y);
                            rooms[createdLastIteration[j].x - 1, createdLastIteration[j].y].GetComponent<Room>().OpenDoor(Room.Direction.right);
                            rooms[createdLastIteration[j].x, createdLastIteration[j].y].GetComponent<Room>().OpenDoor(Room.Direction.left);
                        }
                    }
                    else
                    {
                        if (Random.Range(0, 1) > roomDoorOpenChance)
                        {
                            rooms[createdLastIteration[j].x - 1, createdLastIteration[j].y].GetComponent<Room>().OpenDoor(Room.Direction.right);
                            rooms[createdLastIteration[j].x, createdLastIteration[j].y].GetComponent<Room>().OpenDoor(Room.Direction.left);
                        }
                    }
                }
            }

            //Pitää muuttaa newCreated createdLastiksi
            createdLastIteration.Clear();
            createdLastIteration = new List<Position>(newCreated);
            newCreated.Clear();
        }

    Finish:;
    }


    List<Position> createdLastIteration = new List<Position>();
    List<Position> newCreated = new List<Position>();

    public void Start()
    {
        instance = this;

        InitLevel();

        ConstructLevel(1);

    }

    public void InitLevel()
    {

    }



    public void SetMinimapUnvisited(Position pos, byte possibleDoors)
    {
        //Ylös
        if (rooms[pos.x, pos.y + 1] != null && (rooms[pos.x, pos.y + 1].GetComponent<Room>().doorsOpen & 1 << 2) != 0)
        {
            rooms[pos.x, pos.y + 1].GetComponent<Room>().minimapLayerUnvisited.SetActive(true);
        }
        //Oikee
        if (rooms[pos.x + 1, pos.y] != null && (rooms[pos.x + 1, pos.y].GetComponent<Room>().doorsOpen & 1 << 3) != 0)
        {
            rooms[pos.x + 1, pos.y].GetComponent<Room>().minimapLayerUnvisited.SetActive(true);
        }
        //Alas
        if (rooms[pos.x, pos.y - 1] != null && (rooms[pos.x, pos.y - 1].GetComponent<Room>().doorsOpen & 1 << 0) != 0)
        {
            rooms[pos.x, pos.y - 1].GetComponent<Room>().minimapLayerUnvisited.SetActive(true);
        }
        //Vasen
        if (rooms[pos.x - 1, pos.y] != null && (rooms[pos.x - 1, pos.y].GetComponent<Room>().doorsOpen & 1 << 1) != 0)
        {
            rooms[pos.x - 1, pos.y].GetComponent<Room>().minimapLayerUnvisited.SetActive(true);
        }
    }

    public GameObject[] GetNeighbors(Position pos, byte possibleDoors)
    {
        GameObject[] neighbors = new GameObject[4];
        
        return neighbors;
        
    }

    public byte CheckPossibleNeighbors(int column, int row)
    {
        byte neighbors = 0;

        neighbors = rooms[column, row].GetComponent<Room>().possibleDoors;

        if (rooms[column, row + 1] != null && (neighbors & (1 << 0)) != 0)
        {
            neighbors -= 1;
        }

        if (rooms[column + 1, row] != null && (neighbors & (1 << 1)) != 0)
        {
            neighbors -= 2;
        }

        if (rooms[column, row - 1] != null && (neighbors & (1 << 2)) != 0)
        {
            neighbors -= 4;
        }

        if (rooms[column - 1, row] != null && (neighbors & (1 << 3)) != 0)
        {
            neighbors -= 8;
        }

        return neighbors;
    }

    //public void CreateRandomLevel(int width, int height)
    //{

    //    List<Bit[]> tempMap = new List<Bit[]>();
    //    for (int i = 0; i < width; i++)
    //    {
    //        tempMap.Add(new Bit[height]);
    //        for (int j = 0; j < height; j++)
    //        {

    //            if (Random.Range(0f,1f) < 0.5f)
    //            {
    //                tempMap[i][j] = Bit.empty;
    //            }
    //            else
    //            {
    //                tempMap[i][j] = Bit.terrain;
    //            }
    //        }
    //    }

    //    mappi = tempMap;
    //}
}


public struct Position
{
    public int x;
    public int y;

    public Position(int x, int y)
    {
        this.x = x;
        this.y = y;
    }
}

