﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HealthbarManager : MonoBehaviour
{

    private static HealthbarManager instance;

    public static HealthbarManager Instance
    {
        get
        {
            if (instance == null)
            {
                instance = new HealthbarManager();
            }
            return instance;
        }
    }

    public GameObject hpBar;
    public Image healthBarImage;
    public Sprite[] healthBarSprites;

    // Use this for initialization
    void Start()
    {
        instance = this;
    }

    // Update is called once per frame
    void Update()
    {

    }


    public void InitHealthBar()
    {

    }

    public void SetHpAmount(float hp)
    {
        //Hack pelaajan hpt voivat mennä negatiivisiksi
        healthBarImage.sprite = healthBarSprites[int.Parse(hp.ToString()) < 0 ? 0 : int.Parse(hp.ToString())];
    }
}
