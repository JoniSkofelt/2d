﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

[System.Serializable]
public class PlayerData
{
    private int playerXp = 0;
    public int PlayerXp
    {
        get { return playerXp; }
        set
        {
            playerXp += value;
            if (playerXp > PlayerXpToNextLevel)
            {
                playerXp -= PlayerXpToNextLevel;
                PlayerLevelUp();
            }
        }
    }

    private int playerXpToNextLevel = 0;
    public int PlayerXpToNextLevel { get; set; }

    private int playerLevel = 0;
    public int PlayerLevel { get; set; }

    private int playerHealth = 0;
    public int PlayerHealth { get; set; }


    public void PlayerLevelUp()
    {
        //TODO event lauetaan, joka ohjaa hahmon level up ilmoitusta
        PlayerLevel++;
    }

    public void InitiateStats()
    {
        LoadPlayerData();
    }

    public void SavePlayerData()
    {
        BinaryFormatter bf = new BinaryFormatter();
        FileStream file = File.Open(Application.persistentDataPath + "/playerData.dat", FileMode.OpenOrCreate);
        PlayerData data = new PlayerData();
        bf.Serialize(file, data);
        file.Close();
    }

    public void LoadPlayerData()
    {
        if (File.Exists(Application.persistentDataPath + "/playerData.dat"))
        {
            BinaryFormatter bf = new BinaryFormatter();
            FileStream file = File.Open(Application.persistentDataPath + "/playerData.dat", FileMode.Open);
            PlayerData data = (PlayerData)bf.Deserialize(file);
            file.Close();
            //maxHpSkillLevel = data.maxHpSkillLevel;
        }
    }

    private void OnApplicationQuit()
    {
        SavePlayerData();
    }
}
