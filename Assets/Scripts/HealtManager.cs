﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealtManager : MonoBehaviour
{

    public float playerMaxHp;

    private float playerCurrentHp;

    public float PlayerCurrentHp
    {
        get { return playerCurrentHp; }
        set
        {
            
            HealthbarManager.Instance.SetHpAmount(value);
            playerCurrentHp = value;
            if (playerCurrentHp <= 0)
            {
                GameManager.Instance.CharacterDied();
            }
        }
    }


    // Use this for initialization
    void Start()
    {
        //TODO
        playerMaxHp = SkillStats.maxHp[1];
        playerCurrentHp = playerMaxHp;
    }

    // Update is called once per frame
    void Update()
    {

    }


    public void TakeDamage(float dmg)
    {
        PlayerCurrentHp -= dmg;
    }
}
