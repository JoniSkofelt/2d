﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WindowManager : MonoBehaviour
{
    public GameObject defeatWindowPrefab;
    GameObject defeatWindow;

    // Use this for initialization
    void Start()
    {
        defeatWindow = Instantiate(defeatWindowPrefab, transform);
        defeatWindow.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            ShowDefeatWindow();
        }
    }

    private void OnEnable()
    {
        GameManager.OnDeath += ShowDefeatWindow;
    }

    private void OnDisable()
    {
        GameManager.OnDeath -= ShowDefeatWindow;
    }

    void ShowDefeatWindow()
    {
        defeatWindow.SetActive(true);
    }
}
