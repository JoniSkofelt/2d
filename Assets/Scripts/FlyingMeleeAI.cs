﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlyingMeleeAI : MonoBehaviour
{


	public GameObject player;
	public float dist;
	public float speed = 3;
	public float step;
	public float cd = 1;
	Vector3 point;
	public LayerMask layermask;

	public bool onPoint = false;

	// Use this for initialization
	void Start()
	{
		player = GameObject.FindGameObjectWithTag("Player");
	}

	// Update is called once per frame
	void Update()
	{
		if (onPoint == false) {
			moveAway();
		} else {
			FlyingAttack();
		}
	}

	void Attack()
	{
		
			player.GetComponent<PlayerController> ().TakeDamage (1);
			Debug.Log ("reee");
		}


	void FlyingAttack()
	{
			step = speed * 5 * Time.deltaTime;
			transform.position = Vector2.MoveTowards (transform.position, point, step);
		if (transform.position == point) {
			onPoint = false;
		}
	}

	void moveAway()
	{
		step = speed * Time.deltaTime;
		if (player.transform.position.x < transform.position.x) {
			point = new Vector2 (player.transform.position.x + 4, player.transform.position.y + 4);
		} else {
			point = new Vector2 (player.transform.position.x - 4, player.transform.position.y + 4);
		}
		transform.position = Vector2.MoveTowards(transform.position, point, step);

		RaycastHit2D hit = Physics2D.Raycast (transform.position, player.transform.position - transform.position, 10 ,layermask);
		dist = Vector2.Distance(player.transform.position, transform.position);

		if (dist > 4 && dist < 4.5) {
			if(hit.transform.tag == "Player" && attackOnCooldown == false){
				point = player.transform.position;
				StartCoroutine (AttackOnCooldown ());
				onPoint = true;
			}
		}
	}

	public bool attackOnCooldown = false;

	public IEnumerator AttackOnCooldown()
	{
		attackOnCooldown = true;
		yield return new WaitForSeconds(3);
		attackOnCooldown = false;
	}

	void OnTriggerStay(Collider other)
	{
		print (other.name);
		if (other.tag == "Player") {
			Attack ();
		}
	}
}
