﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlatformPlayerCheck : MonoBehaviour
{
    
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Player")
        {
            transform.parent.GetComponent<Platform>().InsidePlayerChecker(true);
            transform.parent.GetComponent<Platform>().DisableCollider();
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.tag == "Player")
        {
            transform.parent.GetComponent<Platform>().InsidePlayerChecker(false);
            transform.parent.GetComponent<Platform>().TurnColliderOn();
        }
    }
}
