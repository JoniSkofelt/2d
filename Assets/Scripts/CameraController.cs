﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    public Camera mainCamera;
    public Camera minimapCamera;

    public GameObject roomIn;

    public float cameraMoveSensitivity = 10;
    public float cameraMoveDistance = 1;
    // Use this for initialization
    void Start()
    {
        cameraHeight = Camera.main.orthographicSize * 2.0f;
        cameraWidth = cameraHeight * Screen.width / Screen.height;
    }

    Vector3 mousePos;
    Vector3 roomPos;

    float cameraHeight;
    float cameraWidth;

    int roomWidth;
    int roomHeight;

    // Update is called once per frame
    void Update()
    {
        mousePos = Input.mousePosition;
        mousePos = Vector2.ClampMagnitude((Camera.main.ScreenToWorldPoint(mousePos) - transform.position) / cameraMoveSensitivity, cameraMoveDistance);



        //HACK tää pitäs hakee muualta
        mousePos.z = -10;
        mainCamera.transform.localPosition = mousePos;
        //TODO nämä raja-arvot pitäisi hakea muualta
        mainCamera.transform.position = new Vector3(Mathf.Clamp(mainCamera.transform.position.x, roomPos.x - roomWidth + cameraWidth / 2, roomPos.x + roomWidth - cameraWidth / 2),
            Mathf.Clamp(mainCamera.transform.position.y, roomPos.y - roomHeight + cameraHeight / 2, roomPos.y + roomHeight - cameraHeight / 2), mainCamera.transform.position.z);
    }

    
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.layer == LayerMask.NameToLayer("Room"))
        {
            roomIn = collision.gameObject;

            Room temp = collision.GetComponent<Room>();

            roomWidth = temp.width / 2;
            roomHeight = temp.height / 2;

            roomPos = roomIn.transform.position;
        }
    }
}
