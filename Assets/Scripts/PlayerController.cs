﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum RunningDirection
{
    left = 0,
    right = 1
}

public class PlayerController : MonoBehaviour
{

    public float moveSpeed = 5f;
    public float jumpForce = 3f;
    public bool canDoubleJump = true;
    public RunningDirection runningDirection;
    public float moveX;
    float y;
    public bool jump;

    Rigidbody2D rigi;

    // Use this for initialization
    void Start()
    {
        rigi = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        moveX = Input.GetAxis("Horizontal");
        if (moveX != 0)
        {
            runningDirection = moveX < 0 ? RunningDirection.left : RunningDirection.right;
            GetComponent<SpriteRenderer>().flipX = runningDirection == RunningDirection.left;
            GetComponent<Animator>().SetBool("Running", true);
        }
        else
        {
            GetComponent<Animator>().SetBool("Running", false);
        }
        Move();
    }

    RaycastHit2D playerGroundCheckHit;

    public void Move()
    {
        jump = Input.GetButtonDown("Jump");
        //Pudottaudutaan alustan läpi
        if (Input.GetKeyDown(KeyCode.S))
        {
            playerGroundCheckHit = Physics2D.Linecast(transform.position, groundCheck.position, 1 << LayerMask.NameToLayer("Platform"));
            if (playerGroundCheckHit)
            {
                playerGroundCheckHit.transform.GetComponent<Platform>().DisableCollider();
                canDoubleJump = true;
                StartCoroutine(PutJumpOnCooldown());
            }
        }
        //Normaali hyppy kun seistään alustalla
        if (!jumpOnCooldown && jump && CanJump() && !insideTrigger)
        {
            y = jumpForce;
            StartCoroutine(PutJumpOnCooldown());
        }
        //Tuplahyppy ilmassa
        else if (canDoubleJump && jump)
        {
            if(rigi.velocity.y > 0)
            {
                y = rigi.velocity.y + jumpForce;
            }
            else
            {
                y = jumpForce;
            }
            canDoubleJump = false;
        }
        else
        {
            y = rigi.velocity.y;
        }

        rigi.velocity = new Vector2(moveSpeed * moveX, y);

    }

    public Transform groundCheck;
    public bool jumpOnCooldown = false;
    bool grounded;
    public bool insideTrigger;

    public bool CanJump()
    {
        grounded = Physics2D.Linecast(transform.position, groundCheck.position, 1 << LayerMask.NameToLayer("Ground") | 1 << LayerMask.NameToLayer("Platform"));
        if (grounded)
            canDoubleJump = true;
        return grounded;
    }

    public WaitForSeconds jumpCooldown = new WaitForSeconds(0.5f);

    public IEnumerator PutJumpOnCooldown()
    {
        jumpOnCooldown = true;
        yield return jumpCooldown;
        jumpOnCooldown = false;
    }

    //TODO nämä voisi siirtää platform.cs tiedostoon
    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.layer == LayerMask.NameToLayer("Platform") && collision.GetType().Equals(new BoxCollider2D().GetType()))
        {
            collision.GetComponent<Platform>().InsidePlatform(false);
            collision.GetComponent<Platform>().TurnColliderOn();

            insideTrigger = false;
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.layer == LayerMask.NameToLayer("Platform") && collision.GetType().Equals(new BoxCollider2D().GetType()))
        {
            collision.GetComponent<Platform>().InsidePlatform(true);

            insideTrigger = true;
        }
        else if (collision.gameObject.layer == LayerMask.NameToLayer("Room"))
        {
            collision.GetComponent<Room>().PlayerEnteredRoom();
        }
    }

    public void TakeDamage(float amount)
    {
        GetComponent<HealtManager>().TakeDamage(amount);
    }

    //private void OnEnable()
    //{
    //    GameManager.OnDeath += PlayerDied;
    //}

    //private void OnDisable()
    //{
    //    GameManager.OnDeath -= PlayerDied;
    //}

    //void PlayerDied()
    //{

    //}



}
