﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Platform : MonoBehaviour
{
    BoxCollider2D collider;
    public EdgeCollider2D edgeCollider;
    // Use this for initialization
    void Start()
    {
        collider = GetComponent<BoxCollider2D>();
    }

    // Update is called once per frame
    void Update()
    {

    }

    bool insidePlatform = false;
    bool insidePlayerChecker = false;

    public void InsidePlatform(bool value)
    {
        insidePlatform = value;
    }

    public void InsidePlayerChecker(bool value)
    {
        insidePlayerChecker = value;
    }

    public void DisableCollider()
    {
        collider.isTrigger = true;
    }

    public void TurnColliderOn()
    {
        if (!insidePlatform && !insidePlayerChecker)
        {
            collider.isTrigger = false;
        }
    }
}
