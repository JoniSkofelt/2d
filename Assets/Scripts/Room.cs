﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Room : MonoBehaviour
{

    public int width = 30;
    public int height = 20;
    
    public Position posInRoomTable;

    private bool checkedRoom;


    public GameObject minimapLayer;
    public GameObject minimapLayerUnvisited;

    public bool CheckedRoom
    {
        get { return checkedRoom; }
        set
        {
            checkedRoom = value;
            minimapLayer.SetActive(true);
            LevelManager.Instance.SetMinimapUnvisited(posInRoomTable, doorsOpen);
        }
    }

    public bool EnemiesKilled = false;

    public byte possibleDoors = 15;


    //TODO tämä varmaan pitäävaihtaa jossain vaiheessa kun peliin lisätään että huoneet ovat lukittuja kunnes viholliset on tapettu
    public byte doorsOpen;


    public GameObject[] doors;

    public void OpenDoor(Direction dir)
    {
        switch (dir)
        {
            case Direction.up:
                if ((doorsOpen & 1 << 0) == 0)
                {
                    doorsOpen += 1;
                    doors[0].SetActive(false);
                }
                break;
            case Direction.right:
                if ((doorsOpen & 1 << 1) == 0)
                {
                    doors[1].SetActive(false);
                    doorsOpen += 2;
                }
                break;
            case Direction.down:
                if ((doorsOpen & 1 << 2) == 0)
                {
                    doors[2].SetActive(false);
                    doorsOpen += 4;
                }
                break;
            case Direction.left:
                if ((doorsOpen & 1 << 3) == 0)
                {
                    doors[3].SetActive(false);
                    doorsOpen += 8;
                }
                break;
            default:
                break;
        }
    }

    public void PlayerEnteredRoom()
    {
        CheckedRoom = true;
    }


    public enum Direction
    {
        up = 1,
        right = 2,
        down = 3,
        left = 4
    }
    
}
