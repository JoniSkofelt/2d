﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    
    public delegate void GameStateChange();

    public static GameStateChange OnDeath;

    public enum GameState
    {
        Town,
        InGame
    }

    public int currentLevel = 1;

    private static GameManager instance;

    public static GameManager Instance
    {
        get { return instance; }
    }


    // Use this for initialization
    void Awake()
    {
        if (instance == null)
        {
            DontDestroyOnLoad(gameObject);
            instance = this;
        }
        else if(instance != this)
        {
            Destroy(gameObject);
        }
    }

    public void CharacterDied()
    {
        OnDeath();
    }

    public void RestartLevel()
    {
        LoadLevel(currentLevel);
    }

    public void LoadLevel(int level)
    {
        LevelManager.Instance.ConstructLevel(level);
    }
}
